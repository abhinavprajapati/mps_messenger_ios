﻿using System;
using System.Collections.Generic;
using MPSiOSMessenger.Models;
using UIKit;

namespace MPSiOSMessenger.iOS
{
    public partial class ViewController : UIViewController
    {
        int count = 1;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            List<ChatModel> chatModels = new List<ChatModel>();
            chatModels.Add(new ChatModel() { Header = "Group1", LastMesseg = "Group1 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group2", LastMesseg = "Group2 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group2", LastMesseg = "Group3 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group3", LastMesseg = "Group4 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group5", LastMesseg = "Group5 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group6", LastMesseg = "Group6 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group7", LastMesseg = "Group7 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group8", LastMesseg = "Group8 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group9", LastMesseg = "Group9 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group10", LastMesseg = "Group10 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group11", LastMesseg = "Group11 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group12", LastMesseg = "Group12 Last Messege" });
            chatModels.Add(new ChatModel() { Header = "Group13", LastMesseg = "Group13 Last Messege" });
        
            // Perform any additional setup after loading the view, typically from a nib.
            //Button.AccessibilityIdentifier = "myButton";
            //Button.TouchUpInside += delegate
            //{
            //    var title = string.Format("{0} clicks!", count++);
            //    Button.SetTitle(title, UIControlState.Normal);
            //};
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.		
        }
    }
}
